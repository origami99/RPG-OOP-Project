﻿using System.Reflection;
using Core.Interfaces;
using Autofac;
using SunshineConsole;
using Core.Windows;
using Core.Logger;
using AudioPlayerManager;
using System;
using Models;
using AudioPlayerManager.Contracts;

namespace Core
{
    public class Startup
    {
        public static void Main()
        {
            var builder = new ContainerBuilder();
            builder.RegisterAssemblyModules(Assembly.GetExecutingAssembly());
            //builder.RegisterAssemblyModules(Assembly.Load("AudioPlayerManager"));

            //
            builder.Register(c => new ConsoleWindow(Engine.ConsoleHeight, Engine.ConsoleWidth, "OOP-RPG")).AsSelf()
                .SingleInstance();

            builder.RegisterType<WindowsManager>().AsSelf().SingleInstance();
            builder.Register(c => new SelectablePopUpWindow("EnemyInfo", 7, 10, 31, 20, new Symbol('*'), false, 15))
                   .As<SelectablePopUpWindow>().SingleInstance();

            builder.RegisterType<DebugLogger>().As<ILogger>().SingleInstance();

            builder.RegisterType<Player>().As<IPlayer>();
            builder.RegisterType<Audio>().As<IAudio>().SingleInstance();

            builder.RegisterType<Movement>().AsSelf().SingleInstance();
            builder.RegisterType<CombatManager>().AsSelf().SingleInstance();
            builder.RegisterType<Visualization>().AsSelf().SingleInstance();
            builder.RegisterType<KeyHandler>().AsSelf().SingleInstance();
            builder.RegisterType<Random>().AsSelf().SingleInstance();
            builder.RegisterType<MathFunctions>().As<IMathFunctions>().SingleInstance();
            //

            builder.RegisterType<Engine>().As<IEngine>(); //changed Engine to IEngine

            var container = builder.Build();

            var engine = container.Resolve<IEngine>(); // changed Engine to IEngine
            engine.Start();
        }
    }
}
